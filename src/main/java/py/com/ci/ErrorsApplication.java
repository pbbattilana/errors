package py.com.ci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class ErrorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErrorsApplication.class, args);
	}

}

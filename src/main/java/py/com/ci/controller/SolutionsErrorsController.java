package py.com.ci.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import py.com.ci.model.SolutionsErrors;
import py.com.ci.model.SolutionsErrorsData;

/**
 *
 * @author pbbattilana
 */
public interface SolutionsErrorsController {
    ResponseEntity <SolutionsErrorsData> findAll(HttpServletRequest request);
    ResponseEntity <?> findByErrorId(Integer idError, HttpServletRequest request);
    ResponseEntity <?> findBySolutionId(Integer idSolution, HttpServletRequest request);
    ResponseEntity <SolutionsErrors> insert(SolutionsErrors solutionsErrors, HttpServletRequest request);
}

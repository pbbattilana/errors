package py.com.ci.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import py.com.ci.model.Solutions;
import py.com.ci.model.SolutionsData;

/**
 *
 * @author pbbattilana
 */
public interface SolutionsController {    
    ResponseEntity <SolutionsData> findAll(HttpServletRequest request);
    ResponseEntity <?> findById(Integer idSolution, HttpServletRequest request);
    ResponseEntity <Solutions> insert(Solutions solutions, HttpServletRequest request);
    ResponseEntity <?> update(Integer idSolution, Solutions solutions, HttpServletRequest request);
    ResponseEntity <?> delete(Integer idSolution, HttpServletRequest request);
}

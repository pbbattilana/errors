/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package py.com.ci.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import py.com.ci.model.Errors;
import py.com.ci.model.ErrorsGetResData;

/**
 *
 * @author Aldo
 */
public interface ErrorsController {
    
    ResponseEntity <ErrorsGetResData> getErrors(HttpServletRequest request);
    ResponseEntity <Errors> getErrorById(int id, HttpServletRequest request);
    ResponseEntity <Errors> insertError(Errors error, HttpServletRequest request);
    ResponseEntity <Errors> update(py.com.ci.model.Errors errorId, HttpServletRequest request);
    ResponseEntity <Errors> delete(int errorId, HttpServletRequest request);
}

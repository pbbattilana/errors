/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.controller.impl;

import java.util.Objects;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import py.com.ci.controller.ErrorsController;
import py.com.ci.model.Errors;
import py.com.ci.model.ErrorsGetResData;
import py.com.ci.model.exception.ErrorNotFoundException;
import py.com.ci.service.ErrorsService;

/**
 *
 * @author Aldo
 */
@Component
public class ErrorsControllerImpl implements ErrorsController{
    
    private static final Logger LOGGER = LogManager.getLogger(ErrorsControllerImpl.class);
    
    @Autowired
    private ErrorsService errorService;
  

    @Override
    public ResponseEntity<ErrorsGetResData> getErrors(HttpServletRequest request) {
   
        ErrorsGetResData response = errorService.getErrors();
        
        if (Objects.isNull(response)){
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }   

    @Override
    public ResponseEntity<Errors> getErrorById(int id, HttpServletRequest request) {
        
        Errors response;
        try {
            response = errorService.getErrorsById(id);
            return new ResponseEntity<>(response, HttpStatus.OK); 
        } catch (ErrorNotFoundException ex) {
            
            java.util.logging.Logger.getLogger(ErrorsControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @Override
    public ResponseEntity<Errors> insertError(Errors error, HttpServletRequest request) {
        
        Errors errorResponse = errorService.insertError(error);
        
        return new ResponseEntity<>(errorResponse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Errors> update(@PathVariable(value = "id", required = true)
    Errors error, HttpServletRequest request) {
        try {
            errorService.update(error);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (ErrorNotFoundException ex){
            java.util.logging.Logger.getLogger(ErrorsControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);   
            
        }
    }
    
    @Override
    public ResponseEntity<Errors> delete(
            @PathVariable(value = "id", required = true) int errorId,
            HttpServletRequest request
    ) {
        try{
            errorService.delete(errorId);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(ErrorNotFoundException ex){
            LOGGER.error("Error al eliminar error " + errorId + ": " + ex.getMessage());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
   
}

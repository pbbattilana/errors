package py.com.ci.controller.impl;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import py.com.ci.controller.SolutionsErrorsController;
import py.com.ci.model.SolutionsErrors;
import py.com.ci.model.SolutionsErrorsData;
import py.com.ci.model.exception.SolutionErrorNotFoundException;
import py.com.ci.service.SolutionsErrorsService;

/**
 *
 * @author pbbattilana
 */
@Component
public class SolutionsErrorsControllerImpl implements SolutionsErrorsController{
    
    private static final Logger LOGGER = LogManager.getLogger(SolutionsErrorsControllerImpl.class);
    
    @Autowired
    SolutionsErrorsService service;
    
    @Override
    public ResponseEntity<SolutionsErrorsData> findAll(
            HttpServletRequest request
    ){
        SolutionsErrorsData solutionsErrors = service.findAll();
        return new ResponseEntity<>(solutionsErrors, HttpStatus.OK);
    }
    
    @Override
    public ResponseEntity<?> findByErrorId(
            Integer idError,
            HttpServletRequest request
    ){
        try{
            SolutionsErrors solutionError = service.findByErrorId(idError);
            return new ResponseEntity<>(solutionError, HttpStatus.OK);
        }catch(SolutionErrorNotFoundException ex){
            LOGGER.error("Error al buscar el solucionError por idError: " + idError + ": " + ex.getMessage());
            return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
        }
    }
    
    @Override
    public ResponseEntity<?> findBySolutionId(
            Integer idSolution,
            HttpServletRequest request
    ){
        try{
            SolutionsErrors solutionError = service.findBySolutionId(idSolution);
            return new ResponseEntity<>(solutionError, HttpStatus.OK);
        }catch(SolutionErrorNotFoundException ex){
            LOGGER.error("Error al buscar el solucionError por idSolution: " + idSolution + ": " + ex.getMessage());
            return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
        }
    }
    
    @Override
    public ResponseEntity<SolutionsErrors> insert(
            SolutionsErrors solutionError,
            HttpServletRequest request
    ){
        service.insert(solutionError);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.controller.impl;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import py.com.ci.controller.SolutionsController;
import py.com.ci.model.Solutions;
import py.com.ci.model.SolutionsData;
import py.com.ci.model.exception.SolutionNotFoundException;
import py.com.ci.service.SolutionsService;

/**
 *
 * @author pbbattilana
 */
@Component
public class SolutionsControllerImpl implements SolutionsController{

    private static final Logger LOGGER = LogManager.getLogger(SolutionsControllerImpl.class);
    
    @Autowired
    SolutionsService solutionsService;

    @Override
    public ResponseEntity<SolutionsData> findAll(HttpServletRequest request) {
        SolutionsData solutions = solutionsService.findAll();
        return new ResponseEntity<>(solutions, HttpStatus.OK);
    }
    
    @Override
    public ResponseEntity<?> findById(
            Integer idSolution, HttpServletRequest request
    ) {
        try{
            Solutions solution = solutionsService.findById(idSolution);
            return new ResponseEntity<>(solution, HttpStatus.OK);
        }catch(SolutionNotFoundException ex){
            LOGGER.error("Error al buscar la solucion " + idSolution + ": " + ex.getMessage());
            return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
        }
    }
    
    @Override
    public ResponseEntity<Solutions> insert(
            Solutions solution, HttpServletRequest request
    ) {
        solutionsService.insert(solution);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    
    @Override
    public ResponseEntity<?> update(
            Integer idSolution, Solutions solution, HttpServletRequest request
    ) {
        try{
            solutionsService.update(solution);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(SolutionNotFoundException ex){
            LOGGER.error("Error al actualizar la solucion " + solution.getIdSolution() + ": " + ex.getMessage());
            return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
        }
    }
    
    @Override
    public ResponseEntity<?> delete(
            Integer idSolution, HttpServletRequest request
    ) {
        try{
            solutionsService.delete(idSolution);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(SolutionNotFoundException ex){
            LOGGER.error("Error al eliminar la solucion " + idSolution + ": " + ex.getMessage());
            return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
        }
    }
}

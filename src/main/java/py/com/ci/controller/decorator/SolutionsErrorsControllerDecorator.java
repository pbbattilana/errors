package py.com.ci.controller.decorator;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ci.controller.SolutionsErrorsController;
import py.com.ci.controller.impl.SolutionsErrorsControllerImpl;
import py.com.ci.model.SolutionsErrors;
import py.com.ci.model.SolutionsErrorsData;

/**
 *
 * @author pbbattilana
 */
@RestController
@RequestMapping("/solutionserrors")
public class SolutionsErrorsControllerDecorator implements SolutionsErrorsController{
    
    private final SolutionsErrorsController controller;
    
    @Autowired
    public SolutionsErrorsControllerDecorator (SolutionsErrorsController solutionsErrorsController){
        this.controller = solutionsErrorsController;
    }
    
    @Override
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SolutionsErrorsData> findAll(
            HttpServletRequest request
    ){
        return controller.findAll(request);
    }
    
    @Override
    @GetMapping(value = "/error/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByErrorId(
            @PathVariable(value="id", required=true) Integer idError,
            HttpServletRequest request
    ){
        return controller.findByErrorId(idError, request);
    }
    
    @Override
    @GetMapping(value = "/solution/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findBySolutionId(
            @PathVariable(value="id", required=true) Integer idSolution,
            HttpServletRequest request
    ){
        return controller.findBySolutionId(idSolution, request);
    }
    
    @Override
    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SolutionsErrors> insert(
            @RequestBody(required=true) SolutionsErrors solutionError,
            HttpServletRequest request
    ){
        return controller.insert(solutionError, request);
    }
}

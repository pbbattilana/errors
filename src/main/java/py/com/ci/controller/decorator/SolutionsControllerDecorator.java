package py.com.ci.controller.decorator;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ci.controller.SolutionsController;
import py.com.ci.model.Solutions;
import py.com.ci.model.SolutionsData;

/**
 *
 * @author pbbattilana
 */
@RestController
@RequestMapping("/solutions")
public class SolutionsControllerDecorator implements SolutionsController {

    private final SolutionsController controller;
    
    @Autowired
    public SolutionsControllerDecorator (SolutionsController solutionController){
        this.controller = solutionController;
    }
    
    @Override
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SolutionsData> findAll(
            HttpServletRequest request
    ){
        return controller.findAll(request);
    }
    
    @Override
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findById(
            @PathVariable(value="id", required=true) Integer idSolution,
            HttpServletRequest request
    ){
        return controller.findById(idSolution, request);
    }
    
    @Override
    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Solutions> insert(
            @RequestBody(required=true) Solutions solution,
            HttpServletRequest request
    ){
        return controller.insert(solution, request);
    }
    
    @Override
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> update(
            @PathVariable(value="id", required=true) Integer idSolution,
            @RequestBody(required=true) Solutions solution,
            HttpServletRequest request
    ){
        return controller.update(idSolution, solution, request);
    }
    
    @Override
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> delete(
            @PathVariable(value="id", required=true) Integer idSolution,
            HttpServletRequest request
    ){
        return controller.delete(idSolution, request);
    }
}

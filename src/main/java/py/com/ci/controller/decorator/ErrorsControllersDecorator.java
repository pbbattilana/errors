/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.controller.decorator;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.ci.controller.ErrorsController;
import py.com.ci.model.Errors;
import py.com.ci.model.ErrorsGetResData;

/**
 *
 * @author Aldo
 */
@RestController
@RequestMapping("/errors")
public class ErrorsControllersDecorator implements ErrorsController {

    private final ErrorsController errorController;
    
    @Autowired
    public ErrorsControllersDecorator (ErrorsController errorController){
        this.errorController = errorController;
    }
    
    @Override
    @GetMapping(value = "/getErrors", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ErrorsGetResData> getErrors(HttpServletRequest request) {
        return errorController.getErrors(request);
    }
    
    @Override
    @GetMapping(value = "/{id}/getErrorsById", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Errors> getErrorById(@PathVariable(value="id")
        int id, HttpServletRequest request) {
        return errorController.getErrorById(id, request);
    }

    @Override
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Errors> insertError(@RequestBody Errors error, HttpServletRequest request) {
        return errorController.insertError(error, request);
    }
    
    @Override
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Errors> update(@RequestBody
    Errors errorId, HttpServletRequest request){
        return errorController.update(errorId, request);
    }
    
    @Override
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Errors> delete(
            @PathVariable(value="id", required=true) int errorId,
            HttpServletRequest request
    ){
        return errorController.delete(errorId, request);
    }
}

package py.com.ci.converter;

import org.springframework.stereotype.Component;
import py.com.ci.model.Errors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ErrorsConverter {

    public py.com.ci.entity.Error buildEntityFromModel(Errors model){
        py.com.ci.entity.Error entity = new py.com.ci.entity.Error();
        entity.setIdError(model.getId());
        entity.setCodigo(model.getCodigo());
        entity.setEncabezado(model.getEncabezado());
        entity.setLog(model.getLog());
        entity.setTipoError(model.getTipoError());
        return entity;
    }

    public Errors entityToModelOptional(Optional<py.com.ci.entity.Error> entity) {
        if(entity == null) return null;
        Errors model = new Errors();
        model.setId(entity.get().getIdError());
        model.setCodigo(entity.get().getCodigo());
        model.setEncabezado(entity.get().getEncabezado());
        model.setLog(entity.get().getLog());
        model.setTipoError(entity.get().getTipoError());
        return model;
    }

    public List<Errors> entitiesToModels(List<py.com.ci.entity.Error> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        
        List<Errors> errors = new ArrayList<>();
        entities.forEach((entity) -> {
            errors.add(entityToModel(entity));
        });
        return errors;
    }
    
        public Errors entityToModel(py.com.ci.entity.Error entity) {
        if(entity == null) return null;
        Errors model = new Errors();
        model.setId(entity.getIdError());
        model.setCodigo(entity.getCodigo());
        model.setEncabezado(entity.getEncabezado());
        model.setLog(entity.getLog());
        model.setTipoError(entity.getTipoError());
        return model;
    }

}

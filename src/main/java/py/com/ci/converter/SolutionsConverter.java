package py.com.ci.converter;

import org.springframework.stereotype.Component;
import py.com.ci.model.Solutions;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author pbbattilana
 */
@Component
public class SolutionsConverter {

    public py.com.ci.entity.Solution buildEntityFromModel(Solutions model){
        py.com.ci.entity.Solution entity = new py.com.ci.entity.Solution();
        entity.setIdSolution(model.getIdSolution());
        entity.setDescriptionSolution(model.getDescriptionSolution());
        return entity;
    }

    public Solutions entityToModel(py.com.ci.entity.Solution entity) {
        if(entity == null) return null;
        Solutions model = new Solutions();
        model.setIdSolution(entity.getIdSolution());
        model.setDescriptionSolution(entity.getDescriptionSolution());
        return model;
    }
    
    public Solutions entityToModelOptional(Optional<py.com.ci.entity.Solution> entity) {
        if(entity == null) return null;
        Solutions model = new Solutions();
        model.setIdSolution(entity.get().getIdSolution());
        model.setDescriptionSolution(entity.get().getDescriptionSolution());
        return model;
    }

    public List<Solutions> entitiesToModels(List<py.com.ci.entity.Solution> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<Solutions> solutions = new ArrayList<>();
        entities.forEach((entity) -> {
            solutions.add(entityToModel(entity));
        });
        return solutions;
    }

}

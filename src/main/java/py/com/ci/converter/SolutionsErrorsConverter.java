package py.com.ci.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;
import py.com.ci.entity.SolutionError;
import py.com.ci.model.SolutionsErrors;

/**
 *
 * @author pbbattilana
 */

@Component
public class SolutionsErrorsConverter {
    public SolutionError buildEntityFromModel(SolutionsErrors model){
        SolutionError entity = new SolutionError();
        entity.setIdDetalle(model.getIdDetalle());
        entity.setIdError(model.getIdError());
        entity.setIdSolution(model.getIdSolution());
        return entity;
    }

    public SolutionsErrors entityToModel(SolutionError entity) {
        if(entity == null) return null;
        SolutionsErrors model = new SolutionsErrors();
        model.setIdDetalle(entity.getIdDetalle());
        model.setIdError(entity.getIdError());
        model.setIdSolution(entity.getIdSolution());
        return model;
    }

    public SolutionsErrors entityToModelOptional(Optional<SolutionError> entity) {
        if(entity == null) return null;
        SolutionsErrors model = new SolutionsErrors();
        model.setIdDetalle(entity.get().getIdDetalle());
        model.setIdError(entity.get().getIdError());
        model.setIdSolution(entity.get().getIdSolution());
        return model;
    }
    
    public List<SolutionsErrors> entitiesToModels(List<SolutionError> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<SolutionsErrors> solutionsErrors = new ArrayList<>();
        entities.forEach((entity) -> {
            solutionsErrors.add(entityToModel(entity));
        });
        return solutionsErrors;
    }
}

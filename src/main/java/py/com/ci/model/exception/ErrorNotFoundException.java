package py.com.ci.model.exception;

/**
 *
 * @author pbbattilana
 */
public class ErrorNotFoundException extends Exception{
    public ErrorNotFoundException(String message){
        super(message);
    }

}

package py.com.ci.model.exception;

/**
 *
 * @author pbbattilana
 */
public class SolutionErrorNotFoundException extends Exception{
    public SolutionErrorNotFoundException(String message){
        super(message);
    }
}

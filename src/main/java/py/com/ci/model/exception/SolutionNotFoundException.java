package py.com.ci.model.exception;

/**
 *
 * @author pbbattilana
 */
public class SolutionNotFoundException extends Exception{
    public SolutionNotFoundException(String message){
        super(message);
    }
}

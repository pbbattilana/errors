package py.com.ci.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author pbbattilana
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Solutions {

    @JsonProperty(value = "id")
    private Integer idSolution;

    @JsonProperty(value = "descripcion")
    private String descriptionSolution;
}

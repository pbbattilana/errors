package py.com.ci.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author pbbattilana
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolutionsErrors {
    
    @JsonProperty(value = "id")
    private Integer idDetalle;
    
    @JsonProperty(value = "id_error")
    private Integer idError;
    
    @JsonProperty(value = "id_solution")
    private Integer idSolution;
    
}

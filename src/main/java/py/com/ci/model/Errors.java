/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Aldo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Errors implements Serializable{
    
    @JsonProperty(value = "id")
    private Integer id;
    @JsonProperty(value = "codigo")
    private String codigo;
    @JsonProperty(value = "encabezado")
    private String encabezado;
    @JsonProperty(value = "log")
    private String log;
    @JsonProperty(value = "tipo_error")
    private String tipoError;
    
}

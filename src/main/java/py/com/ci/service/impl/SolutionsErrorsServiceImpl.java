package py.com.ci.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ci.converter.SolutionsErrorsConverter;
import py.com.ci.entity.Solution;
import py.com.ci.entity.SolutionError;
import py.com.ci.model.SolutionsErrors;
import py.com.ci.model.SolutionsErrorsData;
import py.com.ci.model.exception.SolutionErrorNotFoundException;
import py.com.ci.repository.SolutionsErrorsRepository;
import py.com.ci.service.SolutionsErrorsService;

/**
 *
 * @author pbbattilana
 */
@Service
public class SolutionsErrorsServiceImpl implements SolutionsErrorsService{

    @Autowired
    SolutionsErrorsRepository repository;

    @Autowired
    SolutionsErrorsConverter converter;
    
    @Override
    public SolutionsErrorsData findAll() {
        SolutionsErrorsData data = new SolutionsErrorsData();
        List<SolutionError> solutionsErrors = (List<SolutionError>) repository.findAll();
        data.setData(converter.entitiesToModels(solutionsErrors));
        return data;
    }

    @Override
    public SolutionsErrors findByErrorId(Integer idError) throws SolutionErrorNotFoundException {
        Optional<SolutionError> optional =  repository.findByErrorId(idError);
        if (optional.isPresent()){
            SolutionsErrors solutionsErrors = converter.entityToModelOptional(optional);
            return solutionsErrors;
        }else{
            throw new SolutionErrorNotFoundException("El ErrorSolucion con idError: " + idError + " no existe");
        }
    }

    @Override
    public SolutionsErrors findBySolutionId(Integer idSolution) throws SolutionErrorNotFoundException {
        Optional<SolutionError> optional =  repository.findBySolutionId(idSolution);
        if (optional.isPresent()){
            SolutionsErrors solutionsErrors = converter.entityToModelOptional(optional);
            return solutionsErrors;
        }else{
            throw new SolutionErrorNotFoundException("El ErrorSolucion con idSolution: " + idSolution + " no existe");
        }
    }

    @Override
    public SolutionsErrors insert(SolutionsErrors model) {
        SolutionError solutionError = repository.save(converter.buildEntityFromModel(model));
        return converter.entityToModel(solutionError);
    }
    
}

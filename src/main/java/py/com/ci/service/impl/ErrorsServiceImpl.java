/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ci.converter.ErrorsConverter;
import py.com.ci.entity.Error;
import py.com.ci.model.Errors;
import py.com.ci.model.ErrorsGetResData;
import py.com.ci.model.exception.ErrorNotFoundException;
import py.com.ci.repository.ErrorsRepository;
import py.com.ci.service.ErrorsService;

/**
 *
 * @author Aldo
 */
@Service
public class ErrorsServiceImpl implements ErrorsService{
        
    @Autowired
    private ErrorsRepository errorsRepository;
    
    @Autowired
    private ErrorsConverter errorsConverter;

    @Override
    public ErrorsGetResData getErrors() {
       
        ErrorsGetResData data= new ErrorsGetResData(); 
        List<Errors> errors = errorsConverter.entitiesToModels((List<Error>) errorsRepository.findAll());
        
        data.setData(errors);
       
        return data;
    }

    @Override
    public Errors getErrorsById(int id) throws ErrorNotFoundException{
 
        Optional<Error> opError =  errorsRepository.findById(id);
        
        if (opError.isPresent()){
            
            Errors error = errorsConverter.entityToModelOptional(opError);
            return error;
        }else{
            throw new ErrorNotFoundException("No se encontró error con id: "+ id);
        }
                        
        
    }

    @Override
    public Errors insertError(Errors error) {
        Error errorEntity = errorsConverter.buildEntityFromModel(error);

        Errors response = errorsConverter.entityToModel(errorsRepository.save(errorEntity));

        return response;

    }

    @Override
    public void update(Errors model) throws ErrorNotFoundException{
        Optional<Error> error = errorsRepository.findById(model.getId());
        if(!error.isPresent()) {
            throw new ErrorNotFoundException("El error " + model.getId()+ " no existe");
        } 
        errorsRepository.save(errorsConverter.buildEntityFromModel(model));
    }
    
    @Override
    public void delete(Integer id) throws ErrorNotFoundException{
        Optional<Error> error = errorsRepository.findById(id);
        if(!error.isPresent()) {
            throw new ErrorNotFoundException("El error " + id + " no existe");
        }   
        errorsRepository.deleteById(id);
    }
}

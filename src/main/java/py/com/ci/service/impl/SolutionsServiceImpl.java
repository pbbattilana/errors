package py.com.ci.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ci.converter.SolutionsConverter;
import py.com.ci.entity.Solution;
import py.com.ci.repository.SolutionsRepository;
import py.com.ci.service.SolutionsService;
import java.util.List;
import java.util.Optional;
import py.com.ci.model.Solutions;
import py.com.ci.model.SolutionsData;
import py.com.ci.model.exception.SolutionNotFoundException;

/**
 *
 * @author pbbattilana
 */

@Service
public class SolutionsServiceImpl implements SolutionsService {

    @Autowired
    SolutionsRepository solutionsRepository;

    @Autowired
    SolutionsConverter solutionsConverter;

    @Override
    public SolutionsData findAll(){
        SolutionsData data = new SolutionsData();
        List<Solution> solutions = (List<Solution>) solutionsRepository.findAll();
        data.setData(solutionsConverter.entitiesToModels(solutions));
        return data;
    }
    
    @Override
    public Solutions findById(Integer id) throws SolutionNotFoundException{
        Optional<Solution> optional =  solutionsRepository.findById(id);
        if (optional.isPresent()){
            py.com.ci.model.Solutions solution = solutionsConverter.entityToModelOptional(optional);
            return solution;
        }else{
            throw new SolutionNotFoundException("La solucion " + id + " no existe");
        }
    }
    
    @Override
    public Solutions insert(Solutions model) {
        Solution solution = solutionsRepository.save(solutionsConverter.buildEntityFromModel(model));
        return solutionsConverter.entityToModel(solution);
    }

    @Override
    public void update(Solutions model) throws SolutionNotFoundException{
        Optional<Solution> solution = solutionsRepository.findById(model.getIdSolution());
        if(!solution.isPresent()) {
            throw new SolutionNotFoundException("La solucion " + model.getIdSolution() + " no existe");
        } 
        solutionsRepository.save(solutionsConverter.buildEntityFromModel(model));
    }
    
    @Override
    public void delete(Integer id) throws SolutionNotFoundException{
        Optional<Solution> solution = solutionsRepository.findById(id);
        if(!solution.isPresent()) {
            throw new SolutionNotFoundException("La solucion " + id + " no existe");
        }   
        solutionsRepository.deleteById(id);
    }

}

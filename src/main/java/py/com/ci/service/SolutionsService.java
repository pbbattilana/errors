package py.com.ci.service;

import py.com.ci.model.Solutions;
import py.com.ci.model.SolutionsData;
import py.com.ci.model.exception.SolutionNotFoundException;

/**
 *
 * @author pbbattilana
 */
public interface SolutionsService {
    public SolutionsData findAll();
    public Solutions findById(Integer id)  throws SolutionNotFoundException;
    public Solutions insert(Solutions model);
    public void update(Solutions model) throws SolutionNotFoundException;
    public void delete(Integer id) throws SolutionNotFoundException;
}

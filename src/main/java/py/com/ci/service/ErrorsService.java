/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package py.com.ci.service;

import py.com.ci.model.Errors;
import py.com.ci.model.ErrorsGetResData;
import py.com.ci.model.exception.ErrorNotFoundException;

/**
 *
 * @author Aldo
 */
public interface ErrorsService {
    
    public ErrorsGetResData getErrors();

    public Errors getErrorsById(int id) throws ErrorNotFoundException;
    
    public Errors insertError(Errors id);
    
    public void update(Errors model) throws ErrorNotFoundException;
    
    public void delete(Integer id) throws ErrorNotFoundException;
}

package py.com.ci.service;

import py.com.ci.model.SolutionsErrors;
import py.com.ci.model.SolutionsErrorsData;
import py.com.ci.model.exception.SolutionErrorNotFoundException;

/**
 *
 * @author pbbattilana
 */
public interface SolutionsErrorsService {
    SolutionsErrorsData findAll();
    SolutionsErrors findByErrorId(Integer idError) throws SolutionErrorNotFoundException;
    SolutionsErrors findBySolutionId(Integer idSolution) throws SolutionErrorNotFoundException;
    SolutionsErrors insert(SolutionsErrors solutionError);
}

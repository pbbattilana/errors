package py.com.ci.repository;

import org.springframework.data.repository.CrudRepository;
import py.com.ci.entity.SolutionError;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface SolutionsErrorsRepository extends CrudRepository<SolutionError, Integer> {

    @Query(
            value = "SELECT * FROM detalle d WHERE d.id_error = ?",
            nativeQuery = true)
    Optional<SolutionError> findByErrorId(Integer idError);

    @Query(
            value = "SELECT * FROM detalle d WHERE d.id_solucion = ?",
            nativeQuery = true)
    Optional<SolutionError> findBySolutionId(Integer idSolution);
}

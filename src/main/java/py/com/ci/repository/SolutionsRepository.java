package py.com.ci.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.ci.entity.Solution;

/**
 *
 * @author pbbattilana
 */
@Repository
public interface SolutionsRepository extends CrudRepository<Solution, Integer>{
    
}

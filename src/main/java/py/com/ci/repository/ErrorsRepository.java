/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package py.com.ci.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.ci.entity.Error;

/**
 *
 * @author Aldo
 */
@Repository
public interface ErrorsRepository extends CrudRepository<Error, Integer>{
    
}

package py.com.ci.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author pbbattilana
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "detalle")
public class SolutionError implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer idDetalle;
    
    @Column(name = "id_error", nullable = true)
    private Integer idError;
    
    @Column(name = "id_solucion", nullable = true)
    private Integer idSolution;
    
    @ManyToOne(cascade = CascadeType.REMOVE)
    private Error error;
    
    @ManyToOne(cascade = CascadeType.REMOVE)
    private Solution solution;
    
}

package py.com.ci.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 *
 * @author pbbattilana
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "solucion")
public class Solution implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Integer idSolution;

    @Basic(optional = true)
    @Column(name = "descripcion", nullable = true)
    private String descriptionSolution;
    
    @OneToMany(mappedBy = "solution", cascade = CascadeType.REMOVE)
    private List<SolutionError> detalles;
}

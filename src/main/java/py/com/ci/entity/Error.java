/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package py.com.ci.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Aldo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "error")
public class Error implements Serializable{
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", nullable = false)
    private Integer idError;

    @Basic(optional = true)
    @Column(name = "codigo", nullable = true)
    private String codigo;
    
    @Basic(optional = true)
    @Column(name = "encabezado", nullable = true)
    private String encabezado;
    
    @Basic(optional = true)
    @Column(name = "log", nullable = true)
    private String log;
    
    @Basic(optional = true)
    @Column(name = "tipo_error", nullable = true)
    private String tipoError;
    
    @OneToMany(mappedBy = "error", cascade = CascadeType.REMOVE)
    private List<SolutionError> detalles;
}
